package intro.androidcr.juniorgram.data.entities

import android.graphics.Bitmap
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.PrimaryKey
import intro.androidcr.juniorgram.data.entities.User
import java.util.*

@Entity(foreignKeys = [ForeignKey(entity = User::class, parentColumns = ["id"], childColumns = ["userId"])])
data class Post(
    @PrimaryKey(autoGenerate = true) val id: Int,
    val photoUrl: String = "",
    val likesCount: Int = 0,
    val description: String = "",
    val publishDate: Date,
    val userId: Int,
    @Ignore val photoBitmap: Bitmap? = null
)