package intro.androidcr.juniorgram.data.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import intro.androidcr.juniorgram.data.entities.Post
import intro.androidcr.juniorgram.data.entities.PostComment

@Dao
interface PostDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(post: Post)

    @Delete
    fun delete(post: Post)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(post: Post)

    @Query("SELECT post.id as postId, user.id as userId, " +
            "user.firstName, user.lastName, user.avatar, comment.id as commentId, comment.comment " +
            "FROM Post INNER JOIN User ON post.userId = user.id INNER JOIN Comment ON post.id = comment.postId" +
            "Where post.id = :postId")
    fun getPostComment(postId: Int): LiveData<List<PostComment>>
}