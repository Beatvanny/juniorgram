package intro.androidcr.juniorgram.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "User")
data class User(
        @PrimaryKey(autoGenerate = true) val id: Int,
        val firstName: String = "",
        val lastName: String = "",
        val avatar: String = "")