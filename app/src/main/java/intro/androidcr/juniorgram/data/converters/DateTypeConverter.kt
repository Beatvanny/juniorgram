package intro.androidcr.juniorgram.data.converters

import androidx.room.TypeConverter
import java.util.*

class DateTypeConverter {
    @TypeConverter
    fun toDate(value: Long?) : Date ? {
        return if (value == null) null else Date(value)
    }

    fun toLong(date: Date?): Long? {
        return date?.time
    }
}
