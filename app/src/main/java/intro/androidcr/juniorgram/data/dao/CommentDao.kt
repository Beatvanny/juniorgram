package intro.androidcr.juniorgram.data.dao

import androidx.room.*
import intro.androidcr.juniorgram.data.entities.Comment

@Dao
interface CommentDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(comment: Comment)

    @Delete
    fun delete(comment: Comment)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(comment: Comment)
}