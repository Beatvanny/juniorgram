package intro.androidcr.juniorgram.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey


@Entity(
    foreignKeys = [
        ForeignKey(
            entity = User::class,
            parentColumns = ["id"],
            childColumns = ["userId"]
        ),
        ForeignKey(
            entity = Post::class,
            parentColumns = ["id"],
            childColumns = ["postId"]
        )]
)
data class Comment(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val comment: String = "",
    val userId: Int = 0,
    val postId: Int = 0
)