package intro.androidcr.juniorgram.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import intro.androidcr.juniorgram.data.converters.DateTypeConverter
import intro.androidcr.juniorgram.data.dao.CommentDao
import intro.androidcr.juniorgram.data.dao.PostDao
import intro.androidcr.juniorgram.data.dao.UserDao
import intro.androidcr.juniorgram.data.entities.Post
import intro.androidcr.juniorgram.data.entities.TimeLine
import intro.androidcr.juniorgram.data.entities.User
import org.w3c.dom.Comment

@Database(
    entities = [User::class, Comment::class, Post::class],
    views = [TimeLine::class],
    version = 1
)

@TypeConverters(DateTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
    abstract fun commentDao(): CommentDao

    companion object {
        var INSTANCE: AppDatabase? = null
        private const val DATABASE_NAME = "DemoDb"

        fun getAppDataBase(context: Context) : AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, DATABASE_NAME).build()
                }
            }
            return INSTANCE
        }
    }
}