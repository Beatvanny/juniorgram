package intro.androidcr.juniorgram.data.entities

import androidx.room.DatabaseView
import java.util.*

@DatabaseView("SELECT user.id as userId, user.firstName, user.lastName, user.avatar, post.id as postId, " +
        "post.photoUrl, post.likesCount, post.description, post.publishDate " +
        "FROM User INNER JOIN post ON user.id = post.userId")
data class TimeLine(
        val userId: Int,
        val firstName: String = "",
        val lastName: String = "",
        val avatar: String = "",
        val postId: Int,
        val photoUrl: String = "",
        val likesCount : Int = 0,
        val description: String = "",
        val publishDate: Date = Date())