package intro.androidcr.juniorgram.data.entities

data class PostComment(
    val postId: Int,
    val userId: Int,
    val firstName: String = "",
    val lastName: String = "",
    val avatar: String = "",
    val commentId: Int,
    val comment: Int
)