package intro.androidcr.juniorgram

import android.content.Intent
import android.graphics.Bitmap
import intro.androidcr.juniorgram.data.entities.Post
import io.reactivex.Observable

interface MainContracts {
    interface View{
        fun addPost(post: Post)
        fun addPosts(posts: List<Post>)
        fun showInputPostDescriptionDialog(photo: Bitmap)
        fun showProgressBar()
        fun hideProgressBar()

    }
    interface Presenter{
        fun getPost()
        fun getAllPosts(userId: Int)
        fun handleActivityForResultPhoto(requestCode: Int, resultCode: Int, data: Intent?)
        fun addPostFromCamera(description: String = "", photo: Bitmap)
    }

    interface Interactor {
        fun getAllItems():Observable<List<Post>>
    }
}