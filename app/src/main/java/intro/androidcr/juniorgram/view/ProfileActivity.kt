package intro.androidcr.juniorgram.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import intro.androidcr.juniorgram.R

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
    }
}
