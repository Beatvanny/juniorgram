package intro.androidcr.juniorgram.view

import android.app.AlertDialog
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.text.InputType
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import intro.androidcr.juniorgram.MainContracts
import intro.androidcr.juniorgram.R
import intro.androidcr.juniorgram.adapter.MainPostsAdapter
import intro.androidcr.juniorgram.data.entities.Post
import intro.androidcr.juniorgram.presenter.MainPresenter
import intro.androidcr.juniorgram.utils.DimenUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.math.roundToInt


class MainActivity : AppCompatActivity(),
    MainContracts.View,
    BottomNavigationView.OnNavigationItemSelectedListener {

    private val mainPresenter = MainPresenter(this)
    private var mainPostsAdapter: MainPostsAdapter? = null

    companion object {
        const val CAMERA_REQUEST = 1888
        const val DATA_KEY = "data"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        bottomNavigation.setOnNavigationItemSelectedListener(this)
        mainPresenter.getAllPosts(1)
    }

    private fun initView() {
        mainPostsAdapter = MainPostsAdapter()
        postsRecyclerView.adapter = mainPostsAdapter
        postsRecyclerView.layoutManager = LinearLayoutManager(this)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.home_button -> {
                true
            }
            R.id.share_photo -> {
                requestPhoto()
                true
            }
            R.id.my_profile -> {
                startActivity(Intent(this@MainActivity, ProfileActivity::class.java))
                true
            }
            else -> false
        }
    }

    override fun showProgressBar() {
        runOnUiThread { progressBarAnimation.visibility = View.VISIBLE }
    }

    override fun hideProgressBar() {
        runOnUiThread { progressBarAnimation.visibility = View.GONE }
    }

    override fun addPost(post: Post) {
        mainPostsAdapter?.let {adapter ->
            adapter.addItem(post)
            postsRecyclerView.scrollToPosition(adapter.itemCount - 1)
        }
    }

    override fun addPosts(posts: List<Post>) {
        runOnUiThread { mainPostsAdapter?.setData(posts) }
    }

    private fun requestPhoto() {
        val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(cameraIntent, CAMERA_REQUEST)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        mainPresenter.handleActivityForResultPhoto(requestCode, resultCode, data)
    }

    override fun showInputPostDescriptionDialog(photo: Bitmap) {
        val inputEditText = EditText(this)
        val photoImageView = ImageView(this)
        AlertDialog.Builder(this)
            .setView(getDialogLinearLayout(photo, photoImageView, inputEditText))
            .setNegativeButton(R.string.input_post_negative_button_text)
            { dialog, _ ->
              dialog.dismiss()
            }
            .setPositiveButton(R.string.input_post_positive_button_text)
            { dialog, _ ->  dialog?.dismiss()
                mainPresenter.addPostFromCamera(inputEditText.text.toString(), photo) }
            .show()
    }

    private fun getDialogLinearLayout(photo: Bitmap, photoImageView: ImageView, inputEditText: EditText): LinearLayout {
        inputEditText.inputType = InputType.TYPE_CLASS_TEXT
        photoImageView.layoutParams =
                ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    DimenUtils.getPxFromDp(this, 200F).roundToInt()
                )
        photoImageView.scaleType = ImageView.ScaleType.FIT_CENTER
        photoImageView.setImageBitmap(photo)
        val linearLayout = LinearLayout(this)
        linearLayout.orientation = LinearLayout.VERTICAL
        linearLayout.addView(photoImageView)
        linearLayout.addView(inputEditText)
        return linearLayout
    }
}
