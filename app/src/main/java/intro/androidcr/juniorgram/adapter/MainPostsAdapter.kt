package intro.androidcr.juniorgram.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import intro.androidcr.juniorgram.R
import intro.androidcr.juniorgram.data.entities.Post
import java.text.SimpleDateFormat
import java.util.*

class MainPostsAdapter() : RecyclerView.Adapter<MainPostViewHolder>() {
    private var posts: ArrayList<Post> = ArrayList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MainPostViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.cell_post_layout, parent, false)
        return MainPostViewHolder(view)
    }

    override fun getItemCount(): Int = posts.size


    override fun onBindViewHolder(holder: MainPostViewHolder, position: Int) {
        val post = posts[position]
        setImage(holder, post)
        holder.likesTextView.text = post.likesCount.toString()
        holder.commentsTextView.text =
                String.format(holder.imageView.context.getString(R.string.post_comments), 40)
        holder.dateTextView.text = getFormattedDate(post.publishDate)
        setComment(holder, post)
        setPostDescription(holder, post)
    }

    private fun setImage(holder: MainPostViewHolder, post: Post) {
        if (post.photoBitmap != null) {
            holder.imageView.setImageBitmap(post.photoBitmap)
        } else {
            Picasso.get().load(post.photoUrl).into(holder.imageView)
        }
    }

    private fun setPostDescription(holder: MainPostViewHolder, post: Post) {
        if (post.description.isNotBlank()) {
            holder.postDescription.visibility = View.VISIBLE
            holder.postDescription.text = post.description
        } else {
            holder.postDescription.visibility = View.GONE
        }
    }

    fun setData(posts: List<Post>) {
        this.posts = ArrayList(posts)
        notifyDataSetChanged()
    }

    fun addItem(post: Post) {
        posts.add(post)
        notifyItemInserted(itemCount)
    }

    private fun setComment(holder: MainPostViewHolder, post: Post) {
        holder.commentsContainer.visibility = View.GONE
        /*if (post.comments.isNotEmpty()) {
            holder.commentsContainer.visibility = View.VISIBLE
            holder.userNameTextView.text = post.comments[0].username
            holder.commentTextView.text = post.comments[0].comment
        } else {
            holder.commentsContainer.visibility = View.GONE
        }*/
    }

    private fun getFormattedDate(date: Date): String {
        val sdf = SimpleDateFormat("dd, MMM yyyy", Locale.US)
        return sdf.format(date)
    }
}