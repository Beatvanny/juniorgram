package intro.androidcr.juniorgram.adapter

import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_post_layout.view.*

class MainPostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val imageView: ImageView = itemView.cell_post_imageView
    val likesTextView: TextView = itemView.cell_post_likes_textView
    val commentsTextView:TextView = itemView.commentsCount
    val dateTextView: TextView = itemView.cell_post_date_textView
    val commentsContainer: LinearLayout = itemView.comment_container
    val userNameTextView: TextView = itemView.comment_username
    val commentTextView: TextView = itemView.comment_text
    val postDescription : TextView = itemView.postDescription
}