package intro.androidcr.juniorgram.interactor

import intro.androidcr.juniorgram.MainContracts
import intro.androidcr.juniorgram.data.entities.Comment
import intro.androidcr.juniorgram.data.entities.Post
import io.reactivex.Observable
import java.util.*

class PostInteractor : MainContracts.Interactor {

    private val posts = listOf(
        Post(
            1,
            "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            50,
            "",
            Date(),
            1
            ),
        Post(
            1,
            "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            50,
            "",
            Date(),
            1
        ),
        Post(
            1,
            "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            50,
            "",
            Date(),
            1
        ),
        Post(
            1,
            "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            50,
            "",
            Date(),
            1
        ),
        Post(
            1,
            "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            50,
            "",
            Date(),
            1
        ),
        Post(
            1,
            "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            50,
            "",
            Date(),
            1
        ),
        Post(
            1,
            "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            50,
            "",
            Date(),
            1
        ),
        Post(
            1,
            "https://www.villascostarica.com/userfiles/playa-manuel-antonio.png",
            50,
            "",
            Date(),
            1
        )
    )

    override fun getAllItems(): Observable<List<Post>> {
        return Observable.fromArray(posts)
    }

}