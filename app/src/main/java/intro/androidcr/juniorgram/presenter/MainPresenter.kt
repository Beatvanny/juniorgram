package intro.androidcr.juniorgram.presenter

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import intro.androidcr.juniorgram.MainContracts
import intro.androidcr.juniorgram.data.entities.Post
import intro.androidcr.juniorgram.interactor.PostInteractor
import intro.androidcr.juniorgram.view.MainActivity.Companion.CAMERA_REQUEST
import intro.androidcr.juniorgram.view.MainActivity.Companion.DATA_KEY
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class MainPresenter(private val view: MainContracts.View) : MainContracts.Presenter {
    private val interactor: MainContracts.Interactor = PostInteractor()

    override fun addPostFromCamera(description: String, photo: Bitmap) {
        val newPost = Post(
            id = 1,
            userId = 1,
            photoUrl = "",
            publishDate = Date(),
            photoBitmap = photo,
            description = description)
        view.addPost(newPost)
    }


    override fun getAllPosts(userId: Int) {
        view.showProgressBar()
        this.interactor.getAllItems()
                .observeOn(Schedulers.io())
                .subscribeOn(AndroidSchedulers.mainThread())
                .delay(5000, TimeUnit.MILLISECONDS)
                .doOnComplete(view::hideProgressBar)
                .subscribe(view::addPosts)
    }

    override fun getPost() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun handleActivityForResultPhoto(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == CAMERA_REQUEST && resultCode == Activity.RESULT_OK && data != null) {
            val photo = data.extras.get(DATA_KEY) as Bitmap
            view.showInputPostDescriptionDialog(photo)
        }
    }
}