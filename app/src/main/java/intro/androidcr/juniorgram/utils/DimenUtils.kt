package intro.androidcr.juniorgram.utils

import android.content.Context
import android.util.TypedValue


object DimenUtils {
    fun getPxFromDp(context: Context, dpValue: Float): Float {
        val resources = context.resources
        return TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            dpValue,
            resources.displayMetrics
        )
    }
}