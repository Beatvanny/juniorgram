package intro.androidcr.juniorgram

import androidx.room.Room
import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4
import intro.androidcr.juniorgram.data.AppDatabase
import intro.androidcr.juniorgram.data.dao.UserDao
import intro.androidcr.juniorgram.data.entities.User
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException

@RunWith(AndroidJUnit4::class)
class UserReadAndWriteTest {

    private lateinit var userDao: UserDao
    private lateinit var db: AppDatabase

    @Before
    fun setup() {
        db = Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(),
            AppDatabase::class.java).build()
        userDao = db.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    @Test
    fun insertUser() {
        var user = User(0, "Gustavo", "Quesada", "")
        userDao.insert(user)

        var users = userDao.getUsers();
        assert(users.value?.isNotEmpty()!!)
    }
}
